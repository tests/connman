#!/bin/sh

set -eu

fail_exit() {
	echo "FAILED"
}
trap fail_exit EXIT

alias grep='grep -q'

MANAGER="busctl --system call net.connman / net.connman.Manager"
TMPDIR=

save_config() {
	if [ -z "$TMPDIR" ] ; then
		TMPDIR=`mktemp -d`
	fi
	cp -r /var/lib/connman $TMPDIR
}

restore_config() {
	if [ -n "$TMPDIR" ] ; then
		systemctl stop connman
		cp -r $TMPDIR/connman /var/lib/
		systemctl start connman
		rm -r $TMPDIR
	fi
}

test_manager() {
	echo test_manager
	${MANAGER} GetProperties > /dev/null
}

test_offlinemode() {
	echo test_offlinemode
	${MANAGER} SetProperty sv "OfflineMode" b true >> /dev/null
	${MANAGER} GetProperties | grep "\"OfflineMode\" b true"
	${MANAGER} SetProperty sv "OfflineMode" b false >> /dev/null
	${MANAGER} GetProperties | grep "\"OfflineMode\" b false"
}

test_technology_enable() {
	local OBJECT_PATH=$1
	echo test_technology_enable ${OBJECT_PATH}

	TECHNOLOGY="busctl --system call net.connman ${OBJECT_PATH} net.connman.Technology"

	# Enable technology if not enabled
	${TECHNOLOGY} GetProperties | grep "\"Powered\" b true" ||
		${TECHNOLOGY} SetProperty sv "Powered" b true

	# Disable technology
	${TECHNOLOGY} SetProperty sv "Powered" b false >> /dev/null

	# Check it is disabled
	${TECHNOLOGY} GetProperties | grep "\"Powered\" b false" ||
		exit 1

	# Enable again
	${TECHNOLOGY} SetProperty sv "Powered" b true >> /dev/null

	sleep 1

	# Check it was enabled properly
	${TECHNOLOGY} GetProperties | grep "\"Powered\" b true"
}


test_technology_tethering() {
	local OBJECT_PATH=$1
	echo test_technology_tethering ${OBJECT_PATH}
	TECHNOLOGY="busctl --system call net.connman ${OBJECT_PATH} net.connman.Technology"
	if ${TECHNOLOGY} GetProperties | grep "\"Tethering\" b true" ; then
		${TECHNOLOGY} GetProperties | grep "TetheringIdentifier" ||
			exit 1
		${TECHNOLOGY} GetProperties | grep "TetheringPassphrase" ||
			exit 1
	fi
}

test_technology_check_property() {
	local PROP_NAME=$1
	echo test_technology_check_property ${PROP_NAME}
	case ${PROP_NAME} in
	Name|Type|Powered|Connected|Tethering|TetheringFreq) ;;
	*) "Invalid property ${PROP_NAME}" ; exit 1 ;;
	esac
}

test_technologies() {
	echo test_technologies
	TECHNOLOGIES=`${MANAGER} GetTechnologies --json=short`
	TECHNOLOGIES=$(echo $TECHNOLOGIES | tr "," " ")
	for WORD in ${TECHNOLOGIES} ; do
		case ${WORD} in
		*"/net/connman/technology/p2p"*)
			echo Skipping ${WORD}
			;;
		*"/net/connman/technology/"*)
			OBJECT_PATH=`echo ${WORD} | sed -e "s/.*\"\(.*\)\".*/\1/"`
			test_technology_enable ${OBJECT_PATH}
			test_technology_tethering ${OBJECT_PATH}
			;;
		*"\":{"*) test_technology_check_property `echo ${WORD} | sed "s/.*\"\(.*\)\":{.*/\1/"`;;
		esac
	done
}

test_service_properties() {
	local OBJECT_PATH=$1
	echo test_service_properties ${OBJECT_PATH}

	SERVICE="busctl --system call net.connman ${OBJECT_PATH} net.connman.Service"

	${SERVICE} SetProperty sv "AutoConnect" b false >> /dev/null
	${SERVICE} GetProperties | grep "\"AutoConnect\" b false"

	# Disconnect service
	${SERVICE} Disconnect  >> /dev/null || true
	sleep 3

	# Make sure service is connected
	${SERVICE} Connect >> /dev/null

	${SERVICE} GetProperties | grep "State"
	${SERVICE} GetProperties | grep "Name"
	${SERVICE} GetProperties | grep "Type"
	${SERVICE} GetProperties | grep "Favorite"
	${SERVICE} GetProperties | grep "Immutable"
	${SERVICE} GetProperties | grep "AutoConnect"
	${SERVICE} GetProperties | grep "Nameservers"
	${SERVICE} GetProperties | grep "Nameservers.Configuration"
	${SERVICE} GetProperties | grep "Timeservers"
	${SERVICE} GetProperties | grep "Timeservers.Configuration"
	${SERVICE} GetProperties | grep "Domains"
	${SERVICE} GetProperties | grep "Domains.Configuration"
	${SERVICE} GetProperties | grep "IPv4"
	${SERVICE} GetProperties | grep "IPv4.Configuration"
	${SERVICE} GetProperties | grep "IPv6"
	${SERVICE} GetProperties | grep "IPv6.Configuration"
	${SERVICE} GetProperties | grep "Proxy"
	${SERVICE} GetProperties | grep "Proxy.Configuration"
	${SERVICE} GetProperties | grep "Provider"
	${SERVICE} GetProperties | grep "Ethernet"

	${SERVICE} SetProperty sv "Timeservers.Configuration" as 3 "0.fedora.pool.ntp.org" "1.fedora.pool.ntp.org" "2.fedora.pool.ntp.org" >> /dev/null

	${SERVICE} SetProperty sv "Nameservers.Configuration" as 2 "4.4.4.4" "8.8.8.8" >> /dev/null

	${SERVICE} SetProperty sv "AutoConnect" b true >> /dev/null
	${SERVICE} GetProperties | grep "\"AutoConnect\" b true"
}

test_ethernet() {
	echo test_ethernet

	SERVICES=`${MANAGER} GetServices`
	for WORD in ${SERVICES} ; do
		case ${WORD} in
		"\"/net/connman/service/ethernet"*)
			OBJECT_PATH=`echo ${WORD} | sed -e "s/.*\"\(.*\)\".*/\1/"`
			test_service_properties ${OBJECT_PATH}
			;;
		esac
	done
}

test_clock() {
	echo test_clock

	CLOCK="busctl --system call net.connman / net.connman.Clock"

	${CLOCK} GetProperties | grep "TimeUpdates"
	${CLOCK} GetProperties | grep "Timezone"
	${CLOCK} GetProperties | grep "Timeservers"
	${CLOCK} GetProperties | grep "TimezoneUpdates"
	TIME=$(${CLOCK} GetProperties | sed -e "s/.*\"Time\" t \([0-9]*\).*/\1/")

	${CLOCK} SetProperty sv "TimeUpdates" s "manual" >> /dev/null
	${CLOCK} SetProperty sv "Time" t ${TIME} >> /dev/null
	${CLOCK} SetProperty sv "Timeservers" as 3  "0.fedora.pool.ntp.org" "1.fedora.pool.ntp.org" "2.fedora.pool.ntp.org" >> /dev/null
	${CLOCK} SetProperty sv "TimeUpdates" s "auto" >> /dev/null
	${CLOCK} SetProperty sv "TimezoneUpdates" s "manual" >> /dev/null
	${CLOCK} SetProperty sv "Timezone" s "America/Vancouver" >> /dev/null
	${CLOCK} SetProperty sv "TimezoneUpdates" s "auto" >> /dev/null
}

save_config
test_manager
test_offlinemode
test_technologies
test_ethernet
. /etc/os-release
if [ "$VARIANT_ID" != "fixedfunction" ];then
	test_clock
fi
restore_config

trap '' EXIT
echo "PASSED"
